<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR001 - InputUserName</name>
   <tag></tag>
   <elementGuidId>f5f0889c-bb5f-4245-b7e0-8779249bd287</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'userName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userName</value>
      <webElementGuid>c582be7e-8214-4fd4-9f82-a3f81f8a6fa1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
