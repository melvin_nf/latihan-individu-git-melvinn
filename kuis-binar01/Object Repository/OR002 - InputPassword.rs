<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR002 - InputPassword</name>
   <tag></tag>
   <elementGuidId>dba46071-6613-4534-8723-fc9601363dfc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>f2dce052-6a6d-46b2-9026-2ef12ed51058</webElementGuid>
   </webElementProperties>
</WebElementEntity>
