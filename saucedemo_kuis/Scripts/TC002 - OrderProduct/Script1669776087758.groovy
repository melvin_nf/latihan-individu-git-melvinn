import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('OrderProduct/OR004 - AddToCart_item'), 0)

Mobile.tap(findTestObject('OrderProduct/OR005 - Cart_icon_button'), 0)

Mobile.tap(findTestObject('OrderProduct/OR006 - Checkout_button'), 0)

Mobile.setText(findTestObject('OrderProduct/OR007 - FirstName_input'), firstName, 0)

Mobile.setText(findTestObject('OrderProduct/OR008 - LastName_input'), lastName, 0)

Mobile.setText(findTestObject('OrderProduct/OR009 - ZipPostalCode_input'), postalCode, 0)

Mobile.tap(findTestObject('OrderProduct/OR010 - Continue_button'), 0)

Mobile.scrollToText('FINISH')

Mobile.tap(findTestObject('OrderProduct/OR011 - Finish_button'), 0)

Mobile.verifyElementText(findTestObject('OrderProduct/TXT001 - THANK YOU FOR YOU ORDER'), 'THANK YOU FOR YOU ORDER')

Mobile.closeApplication()

